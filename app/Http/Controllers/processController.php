<?php

namespace App\Http\Controllers;
// namespace Carbon\Carbon;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;

class processController extends Controller
{
    public function authentication() {
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        $token = json_decode($res);
        // dd($token->access_token);
        
        // tess product
        $clientProduct = new Client();

        $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/etalase?shop_id=10696883", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        
        $productEtalase = json_decode($resProduct);
        // dd($productEtalase);

        // tes order
        $current_time = Carbon::now()->timestamp;
        // 30 menit yang lalu
        $start_time = $current_time - 1800;
        // dd($current_time, $start_time);
        $clientOrder = new Client();
        
        $resOrder = $clientOrder->get("https://fs.tokopedia.net/v2/order/list?fs_id=$appID&shop_id=10696883&from_date=$start_time&to_date=$current_time&page=1&per_page=5&status=200", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $orders = json_decode($resOrder);
        dd($orders, $productEtalase);
    }
}
